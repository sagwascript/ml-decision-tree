#! /usr/bin/python

import numpy as np
from sklearn import datasets

iris = datasets.load_iris()

# load data iris
iris = datasets.load_iris()

# menggabungkan data fitur dengan data target menjadi satu matriks
data = np.concatenate((iris.data, np.array([iris.target]).T), axis=1)

# menggabungkan data hasil pemotongan tiap kelas
x1, x2, x3 = list(data[0:20]), list(data[50:70]), list(data[100:120])
data = np.array(x1 + x2 + x3)

# mendapatkan banyaknya data
n_data = len(data)

# untuk menghitung banyaknya data pada tiap kelas
def distinct(data):
    result = {}
    for row in data:
        label = row[-1]
        if label not in result:
            result[label] = 0
        result[label] += 1
    return result

# Mendapatkan nilai gini impurity
def gini_impurity(data):
    # jika banyaknya data lebih dari 0 maka
    if (len(data) > 0):
        # hitung banyak data tiap kelas
        n_label = distinct(data)
        # definisikan impurity sebagai 1
        impurity = 1
        # lakukan perulangan pada tiap kelas
        for label in n_label:
            # hitung probabilitas suatu kelas terhadap seluruh data
            prob = n_label[label] / float(len(data))
            # kurangkan hasil probabilitas yang sudah dipangkatkan dengan nilai impurity
            impurity -= prob**2
        return impurity
    else:
        return 1

# Fungsi yang digunakan untuk mengambil item yang berbeda pada suatu list
def filter_same_item(data):
    result = []
    for x in data:
        if x not in result:
            result.append(x)
    return result

# Fungsi untuk menghitung nilai tengah pada seluruh data
# parameter data adalah untuk mendefinisikan data yang akan dihitung nilai tengahnya
# parameter feature adalah untuk mendefinisikan fitur mana yang akan dihitung
def median(data, feature):
    # ambil bagian data pada fitur yang dipilih
    all_feature = data[:,feature]
    # urutkan fitur dari yang terkecil
    all_feature.sort()
    
    # filter fitur agar hanya menggunakan nilai yang berbeda saja
    data = filter_same_item(all_feature)
    
    result = []
    # lakukan perulangan pada seluruh data
    for index, value in enumerate(data):
        # jika index data bukan index terakhir maka
        if (index != len(data)-1):
            # tambahkan hasil penjumlahan index pada iterasi ditambah index selanjutnya dibagi dua
            result.append((data[index] + data[index+1]) / 2)
    return result

# State untuk indikator berhentinya perulangan
state = True
# definisikan state sebagai root untuk tracking node pada tree
branch = 'root'
# definisikan treshold, sebagai indikator lain untuk berhenti
treshold = 0
# definisikan variabel next_process untuk menyimpan seluruh data yang akan diproses pada iterasi selanjutnya
next_process = {'gain': 0, 'left_child': [], 'right_child': [], 'cut_value_list': []}
while (state):
    # definisikan list untuk menyimpan gain terbesar pada setiap fitur
    gain_list = []
    # hitung gini impurity pada root
    root = gini_impurity(data)
    # definisikan list untuk menampung data yang masuk kedalam left child
    pick_left_child = []
    # definisikan list untuk menampung data yang masuk kedalam right child
    pick_right_child = []
    # definisikan cut value sebagai 0
    cut_value = 0
    # definisikan feature_idx untuk menyimpan fitur dengan gain tertinggi
    feature_idx = 0
    # definisikan variabel label untuk menyimpan kelas ketika sudah bertemu leaf dari tree
    label = 0
    # lakukan perulangan pada seluruh fitur data
    for i in range(4):
        # hitung median data pada fitur ke-i
        median_data = median(data, i)
        # definisikan biggest_gain sebagai 0
        biggest_gain = 0
        # lakukan perulangan ke seluruh data pada hasil nilai tengah seluruh fitur
        for md in median_data:
            # definisikan left_child sebagai list kosong
            left_child = []
            # definisikan right_child sebagai list kosong
            right_child = []
            # lakukan perulangan pada seluruh baris pada data
            for row in data:
                # jika fitur ke-i pada data lebih kecil dari nilai tengah maka
                if (row[i] <= md):
                    # tambahkan data pada baris tersebut ke dalam left_child
                    left_child.append(row.tolist())
                # jika tidak maka
                else :
                    # tambahkan data pada baris tersebut ke dalam right_child
                    right_child.append(row.tolist())
            # definisikan nilai gain pada left_child menggunakan fungsi gini_impurity dengan memberikan argumen left_child
            gain_left = gini_impurity(left_child)
            # definisikan nilai gain pada right_child menggunakan fungsi gini_impurity dengan memberikan argumen right_child
            gain_right = gini_impurity(right_child)
            # definisikan nilai gain yang didapatkan dari hasil pengurangan gini impurity pada root dikurangi nilai proporsi left child dan nilai proporsi right_child
            gain = root - (len(left_child) / n_data * gain_left) - (len(right_child) / n_data * gain_right)
            
            # print(root, " - ", len(left_child), " / ", n_data, " * ", gain_left, " - ", len(right_child), " / ", n_data, " * ", gain_right)
            
            # jika nilai variabel biggest_gain lebih kecil dari gain yang didapatkan maka
            if (biggest_gain < gain):
                # ubah nilai biggest gain dengan nilai gain yang didapat
                biggest_gain = gain
                # ubah list pada pick_left_child dengan list left_child
                pick_left_child = left_child
                # ubah list pada pick_right_child dengan list right_child
                pick_right_child = right_child
                # ubah nilai variabe cut_value menjadi nilai tengah pada iterasi
                cut_value = md
                # ubah index fitur yang memiliki nilai gain terbesar dengan variabel iterasi i
                feature_idx = i
        
        # tambahkan nilai pada variabel biggest_gain kedalam list gain_list
        gain_list.append(biggest_gain)
    
    # cari item dengan nilai terbesar pada gain_list
    max_gain = max(gain_list)
    # ambil index item dengan nilai terbesar
    idx_max_gain = [i for i, j in enumerate(gain_list) if j == max_gain]
    
    # print proses pembagian data pada masing-masing child
    # print("LEFT")
    # print(pick_left_child)
    # print("RIGHT")
    # print(pick_right_child)
    
    # definisikan list info untuk menyimpan beberapa informasi untuk menggambarkan model tree yang terbentuk
    info = [branch, feature_idx, cut_value]
    
    # ubah nilai gain pada next_process dengan nilai gain terbesar
    next_process['gain'] = max_gain
    # ubah nilai cut value pada 
    next_process['cut_value'] = cut_value
    # ubah list left_child dengan list pada pick_left_child
    next_process['left_child'] = pick_left_child
    # ubah list right_child dengan list pada pick_right_child
    next_process['right_child'] = pick_right_child
            
    # jika banyak data pada left_child lebih besar dari 0 maka
    if (len(next_process['left_child']) > 0):
        # cek jika gini impurity dari left_child sama dengan 0, jika iya maka
        if (gini_impurity(next_process['left_child']) == 0):
            # tambahkan info kelas, karena tree sudah pada posisi leaf atau keputusan
            info.append('class-left:'+str(next_process['left_child'][0][-1]))
            # kosongkan list left_child pada next_process
            next_process['left_child'] = []
            
            # cek jika pada right_child masih terdapat data dan gini impuritynya lebih besar dari treshold, maka
            if(len(next_process['right_child']) > 0 and gini_impurity(next_process['right_child']) > treshold):
                # ubah nilai pada data menjadi list pada right_child
                data = np.array(next_process['right_child'])
                # tambahkan info jika tree pada child left sudah berakhir sedangkan pada posisi right masih berupa node
                info.append('left:end')
                info.append('right:node')
                # ubah branch menjadi right
                branch = 'right'
            # jika tidak maka
            else:
                # tambahkan info kelas, karena tree sudah pada posisi leaf atau keputusan
                info.append('class-right:'+str(next_process['right_child'][0][-1]))
                # tambahkan info jika tree pada child left sudah berakhir sedangkan pada posisi right juga sudah berakhir
                info.append('left:end')
                info.append('right:end')
                # ubah state menjadi false agar perulangan berhenti
                state = False
        # jika tidak maka
        else:
            # ubah nilai pada data menjadi list pada left_child
            data = np.array(next_process['left_child'])
            # tambahkan info jika left_child dan right_child masih berupa node
            info.append('left:node')
            info.append('right:node')
            # ubah branch menjadi left
            branch = 'left'
    # jika banyak data pada right_child lebih besar dari 0 maka
    elif(len(next_process['right_child']) > 0):
        # cek jika gini impurity dari right_child sama dengan 0, jika iya maka
        if (gini_impurity(next_process['right_child']) == 0):
            # tambahkan info kelas, karena tree sudah pada posisi leaf atau keputusan
            info.append('class-right:'+str(next_process['right_child'][0][-1]))
            # kosongkan list right_child pada next_process
            next_process['right_child'] = []
            
            # cek jika pada left_child masih terdapat data dan gini impuritynya lebih besar dari treshold, maka
            if(len(next_process['left_child']) > 0 and gini_impurity(next_process['left_child']) > treshold):
                # ubah nilai pada data menjadi list pada left_child
                data = np.array(next_process['left_child'])
                # ubah branch menjadi right
                branch = 'left'
                # tambahkan info jika tree pada child right sudah berakhir sedangkan pada posisi left masih berupa node
                info.append('left:node')
                info.append('right:end')
            # jika tidak maka
            else:
                # tambahkan info kelas, karena tree sudah pada posisi leaf atau keputusan
                info.append('class-left:'+str(next_process['left_child'][0][-1]))
                # tambahkan info jika tree pada child left sudah berakhir sedangkan pada posisi right juga sudah berakhir
                info.append('left:end')
                info.append('right:end')
                # ubah state menjadi false agar perulangan berhenti
                state = False
        # jika tidak maka
        else:
            # ubah list pada data menjadi list pada right_child
            data = np.array(next_process['right_child'])
            # ubah branch menjadi right
            branch = 'right'
            # info.append('remaining-data:'+str(next_process['right_child']))
            # tambahkan info jika left_child dan right_child masih berupa node
            info.append('left:node')
            info.append('right:node')
    else:
        # ubah state menjadi false karena sudah tidak ada lagi data yang akan diproses
        state = False
    # tambahkan seluruh info pada list cut_value_list
    next_process['cut_value_list'].append(info)

print("===== Tree Model =====")
print(next_process['cut_value_list'])
